const { Router } = require('express');
const request = require('request');
const fs = require('fs');

const { imageMiddleware } = require('../middleware');
const { s3, sharpService } = require('../services');

const router = Router();

router.post('/credentials', imageMiddleware, (req, res) => {
  const { name } = req.body;
  const url = s3.getSignedUrlPutObject(name);

  res.json({ url });
});

router.post('/analysis', (req, res) => {
  const { name } = req.body;
  const url = s3.getSignedUrlGetObject(name);

  const chunks = [];
  request
    .get(url)
    .on('data', async (chunk) => {
      chunks.push(chunk);
    })
    .on('end', async () => {
      const buffer = Buffer.concat(chunks);
      const info = await sharpService.getExifInfo(buffer);
      res.write(JSON.stringify(info));
      const thumbnail = await sharpService.resizeImage(buffer);
      await new Promise((resolve, reject) => {
        fs.writeFile('./output.png', thumbnail, (e) => {
          if (e) {
            return reject(e);
          }

          return resolve();
        });
      });
      res.write(thumbnail);
      res.end();
    })
    .on('error', console.log);
});

module.exports = router;

const btn = document.querySelector('button[type=submit]');
const getCredentials = (file) => {
  const { name, size, type } = file;
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ name, size, type }),
  };

  return fetch('/api/credentials', options).then((res) => res.json());
};

const uploadFile = (file, url) => {
  const options = {
    method: 'PUT',
    body: file,
  };
  return fetch(url, options);
};

const readImageData = async (reader, chunks = []) => {
  const { value, done } = await reader.read();
  if (done) {
    return chunks;
  }
  chunks.push(new Uint8Array(value.buffer));
  return readImageData(reader, chunks);
};

const concatenate = (ResultConstructor, ...arrays) => {
  const totalLength = arrays.reduce((acc, arr) => (acc += arr.length), 0);
  const result = new ResultConstructor(totalLength);

  let offset = 0;
  arrays.forEach((arr) => {
    result.set(arr, offset);
    offset += arr.length;
  });

  return result;
};

const createImageInfoBlock = (info) => {
  const div = document.createElement('div');
  div.style.cssText = 'width:200px;height:200px;';
  div.textContent = info;
  document.body.querySelector('div').appendChild(div);
};

const decodeBufferToText = (buffer) => {
  const chunk = new Uint8Array(buffer);
  return new TextDecoder('utf-8').decode(chunk);
};

const appendImageToDocument = (buffer, format = 'png') => {
  const image = document.querySelector('img');
  image.setAttribute('src', `data:image/${format};base64,${buffer}`);
};

const processChunkedResponse = async (response) => {
  const reader = response.body.getReader();
  const { value } = await reader.read();
  const info = decodeBufferToText(value.buffer);
  createImageInfoBlock(info);

  const bufferingArray = await readImageData(reader);
  const buffer = concatenate(Uint8Array, ...bufferingArray);
  const base64Data = btoa(String.fromCharCode.apply(null, buffer));
  appendImageToDocument(base64Data, info.format);
};

const analysisFile = ({ name }) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ name }),
  };
  return fetch('/api/analysis', options).then(processChunkedResponse);
};

btn.addEventListener('click', async (e) => {
  e.preventDefault();
  const input = document.querySelector('input[type=file]');
  const [file] = input.files;
  try {
    const { url: urlUploadFile } = await getCredentials(file);
    await uploadFile(file, urlUploadFile);
    analysisFile(file);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log("File didn't uploaded", error);
  }
});

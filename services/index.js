const s3 = require('./s3.service');
const sharpService = require('./sharp.service');

module.exports = { s3, sharpService};

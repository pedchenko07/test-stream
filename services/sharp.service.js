const sharp = require('sharp');
const exif = require('exif-reader');

class SharpService {
  static async getExifInfo(chunk) {
    const { format, width, height, exif: exifMeta } = await sharp(
      chunk,
    ).metadata();
    const exifData = exifMeta ? exif(exifMeta) : null;
    return { format, width, height, exifData };
  }

  static resizeImage(image) {
    return sharp(image)
      .resize(100, 100)
      .toBuffer();
  }
}

module.exports = SharpService;

const { S3 } = require('aws-sdk');

const {
  AWS_ACCESS_KEY_ID,
  AWS_SECRET_ACCESS_KEY,
  AWS_REGION,
  AWS_BUCKET,
} = process.env;

class S3Service {
  static get config() {
    return {
      accessKeyId: AWS_ACCESS_KEY_ID,
		  secretAccessKey: AWS_SECRET_ACCESS_KEY,
      region: AWS_REGION,
      Bucket: AWS_BUCKET,
    };
  }

  static getSignedUrlPutObject(filename) {
    const params = { Bucket: AWS_BUCKET, Key: filename, ACL: 'public-read' };
    return this.s3.getSignedUrl('putObject', params);
  }

  static getSignedUrlGetObject(filename) {
    const params = { Bucket: AWS_BUCKET, Key: filename };
    return this.s3.getSignedUrl('getObject', params);
  }
}
S3Service.s3 = new S3(S3Service.config);
module.exports = S3Service;

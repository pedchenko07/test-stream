const {
  IMG_JPEG,
  IMG_JPG,
  IMG_GIF,
} = process.env;

module.exports = (req, res, next) => {
  const { type: mimetype } = req.body;
  const ignore = [IMG_JPEG, IMG_JPG, IMG_GIF];

  const type = mimetype.split('/').pop();

  if (ignore.includes(type)) {
    throw new Error('Wrong type images!');
  }

  next();
};

require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const { api } = require('./routers');

const app = express();
const { PORT: port = 3000 } = process.env;

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);
app.use(express.static('views'));

app.use('/api', api);

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

// eslint-disable-next-line no-console
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
